Simple IP address tools
=======================

Intro
-----

These tools were written for handling sets of files with a varying
number of IP addresses and IP network definitions. The tools are
released in the hope they can be useful for others with similar
needs. They rely heavily on Net::IP and are intended to be run on
Linux, other *nixes will probably do fine as well.

The software consists of three scripts:

  * ipafilter - for best-effort filtering of IP addresses from text files
  * ipacounter - for counting IP addresses
  * ipasets - for creating IP address sets (by removing duplicate addresses)

Run each with option -h for (hopefully) self-explanatory usage instructions.

Requirements
------------

The scripts require the following software to be present:

 * perl 5 - a recent version as of this writing
 * Net::IP - CPAN module
 * Math::BigInt - CPAN module

ipasets writes auxiliary files below /tmp and attempts to clean up after
itself.

Installation
------------

Copy the scripts from below bin to an appropriate place and set
suitable execution rights and ownerships. All scripts assume the
perl interpreter to reside in /usr/bin/perl.

License
-------

MIT(Expat), see LICENSE file. For the required auxiliary software, see their
corresponding licenses.

MS 2020
