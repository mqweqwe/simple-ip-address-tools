Examples with ipacounter
------------------------

user@host:~$ ipacounter -h

A programme to count IP addresses from input files, both IPv4 and IPv6.
If no input file names are given, then stdin is read (v.0.1.4).

  Usage:
    ipacounter [ -4 | -6 | -o outputfile | -h | [ filename [ filename2 ...]] 

    Options:
      -4 = IPv4 addresses and ranges only (default both IPv4 and IPv6)
      -6 = IPv6 addresses and ranges only (default both IPv4 and IPv6)
      -o = output to the given file (default stdout)
      -h = this help


user@host:~$ ipacounter all_sorts.ipv4
785

user@host:~$ cat all_sorts.ipv4 | ipacounter
785

Count IPv6 addresses only:

user@host:~$ ipacounter -6 small_mix.ipv4+6
4

Combining with ipafilter

user@host:~$ ipafilter duplicate_addresses.ipv4 | ipacounter
4

